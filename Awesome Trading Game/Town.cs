﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Awesome_Trading_Game
{
    class Town
    {
        private string name;

        public string getName()
        {
            name = "Boomtown";
            return name;
        }

        public double adjustPrice(double basePrice)
        {
            Random rnd = new Random();
            return basePrice * rnd.Next(1, 3);
        }

    }
}
