﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Awesome_Trading_Game
{
    public partial class Form1 : Form
    {
        private struct tradeItems
        {
            public int itemID;
            public string itemName;
            public double itemPrice;
            public string itemImage;
        }

        tradeItems[] inventory = new tradeItems[6];
        Label[] arrItemNames = new Label[6];
        Label[] arrItemQty = new Label[6];
        Label[] arrItemPrice = new Label[6];

        Town town = new Town();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            createInventory();
            setupScreen();
            loadScreen();

            lblCity.Text = town.getName();

        }

        private void createInventory()
        {
            inventory[0].itemID = 0;
            inventory[0].itemName = "Six Shooter";
            inventory[0].itemPrice = 15.2;
        }

        private void loadScreen()
        {
            arrItemNames[0].Text = inventory[0].itemName;
            arrItemPrice[0].Text = town.adjustPrice(inventory[0].itemPrice).ToString();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void setupScreen()
        {
            arrItemNames[0] = lblItem1;
            arrItemNames[1] = lblItem2;
            arrItemNames[2] = lblItem3;
            arrItemNames[3] = lblItem4;
            arrItemNames[4] = lblItem5;
            arrItemNames[5] = lbItem6;

            arrItemPrice[0] = lblPrice1;
            arrItemPrice[1] = lblPrice2;
            arrItemPrice[2] = lblPrice3;
            arrItemPrice[3] = lblPrice4;
            arrItemPrice[4] = lblPrice5;
            arrItemPrice[5] = lblPrice6;
        }
    }
}
